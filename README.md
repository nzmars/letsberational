# Let's Be Rational

 Let's Be Rational (November 2013; Wilmott, pages 40-53, January 2015) is a follow-up article on By Implication (July 2006). In this newer article, we show how Black's volatility can be implied from option prices with as little as two iterations to maximum attainable precision on standard (64 bit floating point) hardware for all possible inputs.

- [http://www.jaeckel.org](http://www.jaeckel.org/)
- [reference implementation](https://jaeckel.000webhostapp.com/LetsBeRational.7z)

